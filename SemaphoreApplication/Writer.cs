﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemaphoreApplication
{
    class Writer
    {
        Thread writerThread;
        StackThread<string> buffer;
        TextBox textBox;
        TextBox textBoxBuffer;
        bool stop;

        delegate void PrintInTextBox(string str);
        private PrintInTextBox PrintDelegateFunc;
        private PrintInTextBox PrintToBufferFunc;

        public Writer(StackThread<string> buffer, bool stop, TextBox textBox, TextBox textBoxBuffer)
        {
            PrintDelegateFunc = new PrintInTextBox(PrintMsg);
            PrintToBufferFunc = new PrintInTextBox(PrintMsgToBuffer);
            writerThread = new Thread(DoWork);
            this.buffer = buffer;
            this.stop = stop;
            this.textBox = textBox;
            this.textBoxBuffer = textBoxBuffer;
        }

        public void StartWork()
        {
            writerThread.Start();
        }

        private void PrintMsg(string msg)
        {
            if (textBox.InvokeRequired)
                textBox.Invoke(PrintDelegateFunc, new object[] { msg });
            else
                textBox.AppendText(msg);
        }

        private void PrintMsgToBuffer(string msg)
        {
            if (textBoxBuffer.InvokeRequired)
                textBoxBuffer.Invoke(PrintToBufferFunc, new object[] { msg });
            else textBoxBuffer.AppendText(msg);
        }

        public void DoWork()
        {
            while (!stop)
            {
                string message = Const.getRandMessage();
                buffer.Push(message);
                PrintMsg(message + Environment.NewLine);
                PrintMsgToBuffer(message + Environment.NewLine);
                //+ Environment.NewLine + "Количество непрочитанных сообщений: " + buffer.Count);
                Thread.Sleep(new Random().Next(150, 200));
            }
        }

        public bool IsWorking()
        {
            return writerThread.IsAlive;
        }

        public void Stop()
        {
            stop = true;
            writerThread.Abort();
            textBox.BackColor = System.Drawing.Color.FromArgb(255, 255,153,153);
        }
    }
}
