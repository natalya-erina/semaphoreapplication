﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphoreApplication
{
    public struct Const
    {
        public static string getRandMessage()
        {
            return Messages[(new Random()).Next(Messages.Length - 1)];
        }
        public const int maxCapacity = 10;
        public static string[] Messages = { "Thank you", "Hello", "Goodbye", "Great!", "Welcome", "OK", "Message", "Good morning", "Have a nice day", "Sorry", "How are you?" };
    }

    class StackThread<T>
    {
        Stack<T> stack;
        Mutex mut;
        Semaphore empty, busy;
        public int Count { get { return stack.Count; } }
        public StackThread()
        {
            stack = new Stack<T>();
            empty = new Semaphore(Const.maxCapacity, Const.maxCapacity);
            busy = new Semaphore(0, Const.maxCapacity);
            mut = new Mutex();
        }

        public void Push(T elem)
        {
            empty.WaitOne();
            mut.WaitOne();
            stack.Push(elem);
            Thread.Sleep(1500);
            busy.Release();
            mut.ReleaseMutex();
        }

        public T Pop()
        {
            busy.WaitOne();
            mut.WaitOne();
            T elem = stack.Pop();
            Thread.Sleep(1500);
            empty.Release();
            mut.ReleaseMutex();
            return elem;
        }
    }
}
