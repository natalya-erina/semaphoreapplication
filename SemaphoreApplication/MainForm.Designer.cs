﻿namespace SemaphoreApplication
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.textBoxMessages = new System.Windows.Forms.TextBox();
            this.labelBuffer = new System.Windows.Forms.Label();
            this.labelWriter1 = new System.Windows.Forms.Label();
            this.labelWriter2 = new System.Windows.Forms.Label();
            this.labelReader1 = new System.Windows.Forms.Label();
            this.labelReader2 = new System.Windows.Forms.Label();
            this.textBoxWriter1 = new System.Windows.Forms.TextBox();
            this.textBoxReader2 = new System.Windows.Forms.TextBox();
            this.textBoxReader1 = new System.Windows.Forms.TextBox();
            this.textBoxWriter2 = new System.Windows.Forms.TextBox();
            this.buttonStopWriter1 = new System.Windows.Forms.Button();
            this.buttonStopWriter2 = new System.Windows.Forms.Button();
            this.buttonStopReader1 = new System.Windows.Forms.Button();
            this.buttonStopReader2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(22, 22);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxMessages
            // 
            this.textBoxMessages.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxMessages.Location = new System.Drawing.Point(436, 68);
            this.textBoxMessages.Multiline = true;
            this.textBoxMessages.Name = "textBoxMessages";
            this.textBoxMessages.ReadOnly = true;
            this.textBoxMessages.Size = new System.Drawing.Size(116, 265);
            this.textBoxMessages.TabIndex = 1;
            // 
            // labelBuffer
            // 
            this.labelBuffer.AutoSize = true;
            this.labelBuffer.Location = new System.Drawing.Point(433, 52);
            this.labelBuffer.Name = "labelBuffer";
            this.labelBuffer.Size = new System.Drawing.Size(35, 13);
            this.labelBuffer.TabIndex = 2;
            this.labelBuffer.Text = "Buffer";
            // 
            // labelWriter1
            // 
            this.labelWriter1.AutoSize = true;
            this.labelWriter1.Location = new System.Drawing.Point(12, 52);
            this.labelWriter1.Name = "labelWriter1";
            this.labelWriter1.Size = new System.Drawing.Size(44, 13);
            this.labelWriter1.TabIndex = 3;
            this.labelWriter1.Text = "Writer 1";
            // 
            // labelWriter2
            // 
            this.labelWriter2.AutoSize = true;
            this.labelWriter2.Location = new System.Drawing.Point(115, 52);
            this.labelWriter2.Name = "labelWriter2";
            this.labelWriter2.Size = new System.Drawing.Size(44, 13);
            this.labelWriter2.TabIndex = 4;
            this.labelWriter2.Text = "Writer 2";
            // 
            // labelReader1
            // 
            this.labelReader1.AutoSize = true;
            this.labelReader1.Location = new System.Drawing.Point(221, 52);
            this.labelReader1.Name = "labelReader1";
            this.labelReader1.Size = new System.Drawing.Size(51, 13);
            this.labelReader1.TabIndex = 5;
            this.labelReader1.Text = "Reader 1";
            // 
            // labelReader2
            // 
            this.labelReader2.AutoSize = true;
            this.labelReader2.Location = new System.Drawing.Point(327, 52);
            this.labelReader2.Name = "labelReader2";
            this.labelReader2.Size = new System.Drawing.Size(51, 13);
            this.labelReader2.TabIndex = 6;
            this.labelReader2.Text = "Reader 2";
            // 
            // textBoxWriter1
            // 
            this.textBoxWriter1.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxWriter1.Location = new System.Drawing.Point(12, 68);
            this.textBoxWriter1.Multiline = true;
            this.textBoxWriter1.Name = "textBoxWriter1";
            this.textBoxWriter1.ReadOnly = true;
            this.textBoxWriter1.Size = new System.Drawing.Size(100, 265);
            this.textBoxWriter1.TabIndex = 7;
            // 
            // textBoxReader2
            // 
            this.textBoxReader2.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxReader2.Location = new System.Drawing.Point(330, 68);
            this.textBoxReader2.Multiline = true;
            this.textBoxReader2.Name = "textBoxReader2";
            this.textBoxReader2.ReadOnly = true;
            this.textBoxReader2.Size = new System.Drawing.Size(100, 265);
            this.textBoxReader2.TabIndex = 8;
            // 
            // textBoxReader1
            // 
            this.textBoxReader1.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxReader1.Location = new System.Drawing.Point(224, 68);
            this.textBoxReader1.Multiline = true;
            this.textBoxReader1.Name = "textBoxReader1";
            this.textBoxReader1.ReadOnly = true;
            this.textBoxReader1.Size = new System.Drawing.Size(100, 265);
            this.textBoxReader1.TabIndex = 9;
            // 
            // textBoxWriter2
            // 
            this.textBoxWriter2.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxWriter2.Location = new System.Drawing.Point(118, 68);
            this.textBoxWriter2.Multiline = true;
            this.textBoxWriter2.Name = "textBoxWriter2";
            this.textBoxWriter2.ReadOnly = true;
            this.textBoxWriter2.Size = new System.Drawing.Size(100, 265);
            this.textBoxWriter2.TabIndex = 10;
            // 
            // buttonStopWriter1
            // 
            this.buttonStopWriter1.Enabled = false;
            this.buttonStopWriter1.Location = new System.Drawing.Point(12, 339);
            this.buttonStopWriter1.Name = "buttonStopWriter1";
            this.buttonStopWriter1.Size = new System.Drawing.Size(75, 23);
            this.buttonStopWriter1.TabIndex = 11;
            this.buttonStopWriter1.Text = "Stop";
            this.buttonStopWriter1.UseVisualStyleBackColor = true;
            this.buttonStopWriter1.Click += new System.EventHandler(this.buttonStopWriter1_Click);
            // 
            // buttonStopWriter2
            // 
            this.buttonStopWriter2.Enabled = false;
            this.buttonStopWriter2.Location = new System.Drawing.Point(118, 339);
            this.buttonStopWriter2.Name = "buttonStopWriter2";
            this.buttonStopWriter2.Size = new System.Drawing.Size(75, 23);
            this.buttonStopWriter2.TabIndex = 12;
            this.buttonStopWriter2.Text = "Stop";
            this.buttonStopWriter2.UseVisualStyleBackColor = true;
            this.buttonStopWriter2.Click += new System.EventHandler(this.buttonStopWriter2_Click);
            // 
            // buttonStopReader1
            // 
            this.buttonStopReader1.Enabled = false;
            this.buttonStopReader1.Location = new System.Drawing.Point(224, 339);
            this.buttonStopReader1.Name = "buttonStopReader1";
            this.buttonStopReader1.Size = new System.Drawing.Size(75, 23);
            this.buttonStopReader1.TabIndex = 13;
            this.buttonStopReader1.Text = "Stop";
            this.buttonStopReader1.UseVisualStyleBackColor = true;
            this.buttonStopReader1.Click += new System.EventHandler(this.buttonStopReader1_Click);
            // 
            // buttonStopReader2
            // 
            this.buttonStopReader2.Enabled = false;
            this.buttonStopReader2.Location = new System.Drawing.Point(330, 339);
            this.buttonStopReader2.Name = "buttonStopReader2";
            this.buttonStopReader2.Size = new System.Drawing.Size(75, 23);
            this.buttonStopReader2.TabIndex = 14;
            this.buttonStopReader2.Text = "Stop";
            this.buttonStopReader2.UseVisualStyleBackColor = true;
            this.buttonStopReader2.Click += new System.EventHandler(this.buttonStopReader2_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 371);
            this.Controls.Add(this.buttonStopReader2);
            this.Controls.Add(this.buttonStopReader1);
            this.Controls.Add(this.buttonStopWriter2);
            this.Controls.Add(this.buttonStopWriter1);
            this.Controls.Add(this.textBoxWriter2);
            this.Controls.Add(this.textBoxReader1);
            this.Controls.Add(this.textBoxReader2);
            this.Controls.Add(this.textBoxWriter1);
            this.Controls.Add(this.labelReader2);
            this.Controls.Add(this.labelReader1);
            this.Controls.Add(this.labelWriter2);
            this.Controls.Add(this.labelWriter1);
            this.Controls.Add(this.labelBuffer);
            this.Controls.Add(this.textBoxMessages);
            this.Controls.Add(this.buttonStart);
            this.Name = "MainForm";
            this.Text = "Messages";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox textBoxMessages;
        private System.Windows.Forms.Label labelBuffer;
        private System.Windows.Forms.Label labelWriter1;
        private System.Windows.Forms.Label labelWriter2;
        private System.Windows.Forms.Label labelReader1;
        private System.Windows.Forms.Label labelReader2;
        private System.Windows.Forms.TextBox textBoxWriter1;
        private System.Windows.Forms.TextBox textBoxReader2;
        private System.Windows.Forms.TextBox textBoxReader1;
        private System.Windows.Forms.TextBox textBoxWriter2;
        private System.Windows.Forms.Button buttonStopWriter1;
        private System.Windows.Forms.Button buttonStopWriter2;
        private System.Windows.Forms.Button buttonStopReader1;
        private System.Windows.Forms.Button buttonStopReader2;
    }
}

