﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemaphoreApplication
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            buffer = new StackThread<string>();
            writer1 = new Writer(buffer, stop, textBoxWriter1, textBoxMessages);
            writer2 = new Writer(buffer, stop, textBoxWriter2, textBoxMessages);
            rand = new Random();
        }

        Writer writer1;
        Writer writer2;
        Reader reader1;
        Reader reader2;
        Random rand;

        bool stop = false;
        StackThread<string> buffer;

        public void StartProcess()
        {
            writer1.StartWork();
            writer2.StartWork();
            reader1 = new Reader(buffer, 1, stop, textBoxReader1, textBoxMessages);
            reader1.StartWork();
            Thread.Sleep(rand.Next(500, 1000));
            reader2 = new Reader(buffer, 2, stop, textBoxReader2, textBoxMessages);
            reader2.StartWork();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Thread(StartProcess).Start();
            buttonStopReader1.Enabled = true;
            buttonStopReader2.Enabled = true;
            buttonStopWriter1.Enabled = true;
            buttonStopWriter2.Enabled = true;
        }

        private void buttonStopWriter1_Click(object sender, EventArgs e)
        {
            writer1.Stop();
        }

        private void buttonStopWriter2_Click(object sender, EventArgs e)
        {
            writer2.Stop();
        }

        private void buttonStopReader1_Click(object sender, EventArgs e)
        {
            reader1.Stop();
        }

        private void buttonStopReader2_Click(object sender, EventArgs e)
        {
            reader2.Stop();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (writer1.IsWorking())
                writer1.Stop();
            if (writer2.IsWorking())
                writer2.Stop();
            if (reader1 != null && reader1.IsWorking())
                reader1.Stop();
            if (reader2 != null && reader2.IsWorking())
                reader2.Stop();
        }
    }
}
