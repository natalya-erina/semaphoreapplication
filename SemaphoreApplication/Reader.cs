﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemaphoreApplication
{
    class Reader
    {
        Thread readerThread;
        StackThread<string> buffer;
        Random rand;
        TextBox textBox;
        TextBox textBoxBuffer;
        int index;
        bool stop;

        delegate void PrintInTextBox(string str);
        private PrintInTextBox PrintDelegateFunc;

        delegate void DeleteFromTextBox(string str);
        private DeleteFromTextBox DeleteFromTextBoxFunc;

        public Reader(StackThread<string> buffer, int index, bool stop, TextBox textBox, TextBox textBoxBuffer)
        {
            DeleteFromTextBoxFunc = new DeleteFromTextBox(DeleteMsg);
            PrintDelegateFunc = new PrintInTextBox(PrintMsg);
            readerThread = new Thread(DoWork);
            this.buffer = buffer;
            this.index = index;
            this.stop = stop;
            this.textBox = textBox;
            this.textBoxBuffer = textBoxBuffer;
            rand = new Random();
        }

        private void PrintMsg(string msg)
        {
            if (textBox.InvokeRequired)
                textBox.Invoke(PrintDelegateFunc, new object[] { msg });
            else
                textBox.AppendText(msg);
        }

        public void StartWork() { readerThread.Start(); }

        private void DeleteMessage(string message)
        {
            var messages = textBoxBuffer.Lines.ToList();
            List<string> newMessages = new List<string>();
            for (int i = 0; i < messages.Count; i++)
                if (i != messages.Count - 2)
                newMessages.Add(messages[i]);
            textBoxBuffer.Lines = newMessages.ToArray();
        }

        private void DeleteMsg(string message)
        {
            if (textBoxBuffer.InvokeRequired)
                textBoxBuffer.Invoke(DeleteFromTextBoxFunc, new object[] { message });
            else
            {
                List<string> messages = textBoxBuffer.Lines.ToList();
                List<string> newMessages = new List<string>();
                for (int i = 0; i < messages.Count; i++)
                    if (i != messages.Count - 2)
                        newMessages.Add(messages[i]);
                textBoxBuffer.Lines = newMessages.ToArray();
            }
                
        }

        public bool IsWorking()
        {
            return readerThread.IsAlive;
        }

        public void Stop()
        {
            stop = true;
            readerThread.Abort();
            textBox.BackColor = System.Drawing.Color.FromArgb(255, 255, 153, 153);
        }

        public void DoWork()
        {
            while (!stop)
            {
                string message = buffer.Pop();
                PrintMsg(message + Environment.NewLine);
                DeleteMsg(message);
                Thread.Sleep(rand.Next(1000, 5000));
            }
            Thread.Sleep(rand.Next(1500, 2500));
        }
    }
}
